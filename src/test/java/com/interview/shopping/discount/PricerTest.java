package com.interview.shopping.discount;

import org.junit.Test;

import static com.interview.shopping.discount.Catalogue.*;
import static org.assertj.core.api.Assertions.assertThat;

public class PricerTest {

    @Test
    public void shouldPassSimpleCases() {
        checkPrice(0.7f, new Basket(
                APPLE,
                MILK));
        checkPrice(9.99f, new Basket(
                AVENGERS_ASSEMBLE));
        checkPrice(36.54f, new Basket(
                TROUSERS,
                SHIRT));
    }

    @Test
    public void shouldPassTwoForOneDvd() {
        checkPrice(9.99f, new Basket(
                AVENGERS_ASSEMBLE,
                AVENGERS_ASSEMBLE));
        checkPrice(20.0f, new Basket(
                AVENGERS_ASSEMBLE,
                BLUE_PLANET));
        checkPrice(40.0f, new Basket(
                AVENGERS_ASSEMBLE,
                BLUE_PLANET,
                BLUE_PLANET));
        checkPrice(34.95f, new Basket(
                AVENGERS_ASSEMBLE,
                BOND_COLLECTION,
                BOND_COLLECTION,
                BLUE_PLANET));
        checkPrice(29.99f, new Basket(
                AVENGERS_ASSEMBLE,
                AVENGERS_ASSEMBLE,
                BLUE_PLANET));
        checkPrice(34.95f, new Basket(
                AVENGERS_ASSEMBLE,
                BOND_COLLECTION,
                BLUE_PLANET));
    }

    @Test
    public void shouldPassMixedBaskets() {
        checkPrice(49.6f, new Basket(
                AVENGERS_ASSEMBLE,
                APPLE,
                TROUSERS,
                BLUE_PLANET));
        checkPrice(55.28f, new Basket(
                MILK,
                MILK,
                SHIRT,
                SHIRT,
                BLUE_PLANET,
                BLUE_PLANET,
                BLUE_PLANET,
                BLUE_PLANET));
        checkPrice(72.19f, new Basket(
                APPLE,
                MILK,
                TROUSERS,
                SHIRT,
                AVENGERS_ASSEMBLE,
                BOND_COLLECTION,
                BLUE_PLANET));
    }

    private static void checkPrice(float expectedPrice, Basket basket) {
        float price = pricer().price(basket);
        assertThat(price).isEqualTo(expectedPrice);
    }

    private static Pricer pricer() {
        return new SmartPricer();
    }
}
