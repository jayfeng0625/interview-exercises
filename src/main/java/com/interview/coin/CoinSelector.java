package com.interview.coin;

import java.util.ArrayList;
import java.util.List;

public class CoinSelector {

    public List<Integer> calculateCoinsNeeded(List<Integer> coinDenominations, int money) {
        List<Integer> results = new ArrayList<>();
        for (int coin : coinDenominations) {
            while (money >= coin) {
                money -= coin;
                results.add(coin);
            }
        }
        return results;
    }
}
