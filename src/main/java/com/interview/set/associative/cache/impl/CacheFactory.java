package com.interview.set.associative.cache.impl;

import com.interview.set.associative.cache.base.Cache;
import com.interview.set.associative.cache.base.Evictable;
import com.interview.set.associative.cache.base.EvictionStrategy;
import com.interview.set.associative.cache.impl.strategy.BuiltInEvictionStrategy;

import java.util.function.Function;

/**
 * Static factory for creating Set associative cache
 */
public final class CacheFactory {

   /**
    * Creates a new instance with given associative level, the number of segment and an option of built-in eviction strategy.
    * @param blockCount the number of block per segment, this defines the associative level of the cache
    * @param segmentCount the number of segment in the whole cache
    * @param builtInEvictionStrategyOptions either MRU or LRU
    * @param <K> the key associated with the value
    * @param <V> the value to be stored in the entry
    * @return the implementation of n-way set associative cache
    */
   public static <K, V> Cache<K, V> createInstance(int blockCount,
                                                   int segmentCount,
                                                   BuiltInEvictionStrategy builtInEvictionStrategyOptions) {
      return new SetAssociativeCache<>(blockCount, segmentCount, builtInEvictionStrategyOptions.get(), null);
   }

   /**
    * Creates a new instance with given associative level, the number of segment,
    * an option of built-in eviction strategy and a callback that loads the value if not present in cache.
    * @param blockCount the number of block per segment, this defines the associative level of the cache
    * @param segmentCount the number of segment in the whole cache
    * @param evictionStrategy the custom implementation of the eviction strategy
    * @param <K> the key associated with the value
    * @param <V> the value to be stored in the entry
    * @return the implementation of n-way set associative cache
    */
   public static <K, V> Cache<K, V> createInstance(int blockCount,
                                                   int segmentCount,
                                                   EvictionStrategy<? extends Evictable> evictionStrategy) {
      return new SetAssociativeCache<>(blockCount, segmentCount, evictionStrategy, null);
   }

   /**
    * Creates a new instance with given associative level, the number of segment and an custom implementation of eviction strategy.
    * @param blockCount the number of block per segment, this defines the associative level of the cache
    * @param segmentCount the number of segment in the whole cache
    * @param builtInEvictionStrategyOptions either MRU or LRU
    * @param valueLoader callback that loads the value if not present in cache
    * @param <K> the key associated with the value
    * @param <V> the value to be stored in the entry
    * @return the implementation of n-way set associative cache
    */
   public static <K, V> Cache<K, V> createInstance(int blockCount,
                                                   int segmentCount,
                                                   BuiltInEvictionStrategy builtInEvictionStrategyOptions,
                                                   Function<K, V> valueLoader) {
      return new SetAssociativeCache<>(blockCount, segmentCount, builtInEvictionStrategyOptions.get(), valueLoader);
   }

   /**
    * Creates a new instance with given associative level, the number of segment,
    * an custom implementation of eviction strategy and a callback that loads the value if not present in cache.
    * @param blockCount the number of block per segment, this defines the associative level of the cache
    * @param segmentCount the number of segment in the whole cache
    * @param evictionStrategy the custom implementation of the eviction strategy
    * @param valueLoader callback that loads the value if not present in cache
    * @param <K> the key associated with the value
    * @param <V> the value to be stored in the entry
    * @return the implementation of n-way set associative cache
    */
   public static <K, V> Cache<K, V> createInstance(int blockCount,
                                                   int segmentCount,
                                                   EvictionStrategy<? extends Evictable> evictionStrategy,
                                                   Function<K, V> valueLoader) {
      return new SetAssociativeCache<>(blockCount, segmentCount, evictionStrategy, valueLoader);
   }
}
