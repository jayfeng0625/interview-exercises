package com.interview.set.associative.cache.impl.strategy;

import com.interview.set.associative.cache.base.EvictionStrategy;
import com.interview.set.associative.cache.impl.evictable.EvictByUsageFrequency;

import java.util.Collection;
import java.util.Comparator;

/**
 * A implementation of the LFU algorithm that evicts the least frequently used entry from any given collection of {@link EvictByUsageFrequency}
 */
class LeastFrequentlyUsedStrategy implements EvictionStrategy<EvictByUsageFrequency> {

   /**
    * Execute the LFU algorithm on a collection of evictable objects.
    * @param evictables a collection of evictable objects
    */
   @Override
   public void performEviction(Collection<EvictByUsageFrequency> evictables) {
      evictables.stream()
                .min(Comparator.comparingInt(EvictByUsageFrequency::getUsageCount))
                .ifPresent(EvictByUsageFrequency::evict);
   }
}
