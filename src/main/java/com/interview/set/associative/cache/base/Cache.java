package com.interview.set.associative.cache.base;

import java.util.function.Function;

/**
 * Interface for cache with entries mapping from keys to values. Cache entries are manually added using
 * {@link #getOrLoad(Object, Function)} or {@link #put(Object, Object)}, and retrieved using {@link #get(Object)}
 * Values are stored in the cache until either evicted or manually invalidated.
 * @param <K> the key associated with the value
 * @param <V> the value to be stored in the entry
 */
public interface Cache<K, V> {

   /**
    * @param key the key associated with its cached value
    * @return the value associated with {@code key} in this cache
    */
   V get(K key);

   /**
    * @param key the key associated with its cached value
    * @param valueLoader the function to call if the value associated with {@code key} is not found
    * @return the value associated with {@code key} in this cache, this follows the "if cached, then return;
    * otherwise load, cached then return" pattern
    */
   V getOrLoad(K key, Function<K, V> valueLoader);

   /**
    * Associates and store {@code value} with {@code key} in this cache. If the cache previously contained a
    * value associated with {@code key}, the old value should be replaced by {@code value}.
    * @param key the key associated with its cached value
    * @param value the value to be cached
    */
   void put(K key, V value);

   /**
    * @return the number of entries in this cache.
    */
   int size();

   /**
    * Manual eviction of all cache entries
    */
   void evictAll();

}