package com.interview.set.associative.cache.impl.evictable;

import com.interview.set.associative.cache.base.Evictable;

public interface EvictByLastAccessedTime extends Evictable {

    /**
     * @return a timestamp that represents the last accessed time
     */
    long getLastAccessed();
}
