package com.interview.set.associative.cache.impl.strategy;


import com.interview.set.associative.cache.base.Evictable;
import com.interview.set.associative.cache.base.EvictionStrategy;

/**
 * Built-in eviction strategy for cache.
 */
public enum BuiltInEvictionStrategy {

    MRU(new MostRecentUsedStrategy()),
    LRU(new LeastRecentUsedStrategy()),
    LFU(new LeastFrequentlyUsedStrategy());

    EvictionStrategy<? extends Evictable> evictionStrategy;

    BuiltInEvictionStrategy(EvictionStrategy<? extends Evictable> evictionStrategy) {
        this.evictionStrategy = evictionStrategy;
    }

    /**
     * Obtains the implementation of the eviction strategy
     * @return the eviction strategy
     */
    public EvictionStrategy<? extends Evictable> get() {
        return evictionStrategy;
    }

    public static EvictionStrategy<? extends Evictable> parse(String name) {
        try {
            return BuiltInEvictionStrategy.valueOf(name.toUpperCase()).get();
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
