package com.interview.set.associative.cache.impl.evictable;

import com.interview.set.associative.cache.base.Evictable;

public interface EvictByUsageFrequency extends Evictable {

   /**
    * @return a number that represents the amount this object has been used
    */
   int getUsageCount();
}
