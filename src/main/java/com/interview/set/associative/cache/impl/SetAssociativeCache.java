package com.interview.set.associative.cache.impl;

import com.interview.set.associative.cache.base.Cache;
import com.interview.set.associative.cache.base.EvictionStrategy;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * An implementation of n-way set associative cache (SAC)
 * @param <K> the key associated with the value
 * @param <V> the value to be stored in the entry
 */
public class SetAssociativeCache<K, V> implements Cache<K, V> {

   /**
    * The number of block per segment indicates the associativity level
    */
   private final int totalBlockCount;
   private final int segmentCount;
   private final Map<Integer, List<Block>> cache;
   private final Function<K, V> valueLoader;
   private final EvictionStrategy<Block> evictionStrategy;

   /**
    * Creates a new instance with given associative level, the number of segment,
    * an custom implementation of eviction strategy and a callback that loads the value if not present in cache
    */
   @SuppressWarnings("unchecked")
   SetAssociativeCache(int totalBlockCount,
                       int segmentCount,
                       EvictionStrategy evictionStrategy,
                       Function<K, V> valueLoader) {
      this.totalBlockCount = totalBlockCount;
      this.segmentCount = segmentCount;
      this.evictionStrategy = evictionStrategy;
      this.valueLoader = valueLoader;
      this.cache = new ConcurrentHashMap<>(segmentCount);

      this.initialise();
   }

   /**
    * @param key the key associated with its cached value
    * @return the value associated with {@code key} in this cache
    * @throws UnsupportedOperationException throws when the valueLoader is not specified beforehand
    */
   @Override
   public V get(K key) {
      if (valueLoader == null) {
         throw new UnsupportedOperationException("You must specify a value loader in order to use get(key)!");
      }
      return this.getOrLoad(key, valueLoader);
   }

   /**
    * @param key the key associated with its cached value
    * @param valueLoader the function to call if the value associated with {@code key} is not found
    * @return the value associated with {@code key} in this cache, this follows the "if cached, then return;
    * otherwise load, cached then return" pattern
    */
   @Override
   @SuppressWarnings("unchecked")
   public V getOrLoad(K key, Function<K, V> valueLoader) {
      List<Block> segment = findSuitableSegment(key);
      Block block = findSuitableBlock(segment, key);
      if (block == null || block.isEmpty()) {
         V value = valueLoader.apply(key);
         put(key, value);
         return value;
      }
      block.setLastAccessed(System.nanoTime());
      block.increaseHitCount();
      return (V) block.getValue();
   }

   /**
    * Associates and store {@code value} with {@code key} in this cache. It computes the hash of a given key
    * to locate a suitable segment for input. If the segment is full, the eviction process will be executed.
    * Note the eviction does not remove the block from the cache, it only clear the data stored in the block.
    * If the cache previously contained a value associated with {@code key}, the old value should be replaced by {@code value}.
    * @param key the key associated with its cached value
    * @param value the value to be cached
    */
   @Override
   public synchronized void put(K key, V value) {
      Objects.requireNonNull(key, "key cannot be null!");

      List<Block> segment = findSuitableSegment(key);
      if (isSegmentFull(segment)) {
         evictionStrategy.performEviction(segment);
      }

      Block block = findSuitableBlock(segment, key);
      if (block != null) {
         block.setKey(key);
         block.setValue(value);
         block.setLastAccessed(System.nanoTime());
         if (!block.getKey().equals(key)) {
            block.setLastCreated(System.nanoTime());
         }
      }
   }

   /**
    * The size is calculated by the associativity level (number of block per segment),
    * multiply by the number of segment
    * @return the number of entries in this cache.
    */
   @Override
   public int size() {
      return Math.toIntExact(cache.values().stream()
                 .flatMap(Collection::stream)
                 .filter(block -> !block.isEmpty())
                 .count());
   }

   /**
    * Manual eviction of all cache entries, note this does not remove the block from the cache,
    * it only clear the data stored in the block
    */
   @Override
   public void evictAll() {
      cache.values().stream()
                    .flatMap(Collection::stream)
                    .forEach(Block::evict);
   }

   private Block findSuitableBlock(List<Block> segment, K key) {
      return segment.stream()
                    .filter(block -> key.equals(block.getKey()) || block.isEmpty())
                    .findFirst()
                    .orElse(null);
   }

   private List<Block> findSuitableSegment(K key) {
      int segmentIndex = Math.abs(Objects.hash(key) % segmentCount);
      return Collections.unmodifiableList(cache.get(segmentIndex));
   }

   private boolean isSegmentFull(List<Block> segment) {
      return segment.stream()
                    .noneMatch(Block::isEmpty);
   }

   private void initialise() {
      for (int i = 0, len = totalBlockCount * segmentCount; i < len; i++) {
         List<Block> blocks = new ArrayList<>();
         for (int j = 0; j < totalBlockCount; j++) {
            blocks.add(new Block());
         }
         cache.put(i % segmentCount, Collections.synchronizedList(blocks));
      }
   }
}
