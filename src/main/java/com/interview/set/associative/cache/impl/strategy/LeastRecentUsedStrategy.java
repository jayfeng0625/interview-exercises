package com.interview.set.associative.cache.impl.strategy;

import com.interview.set.associative.cache.base.EvictionStrategy;
import com.interview.set.associative.cache.impl.evictable.EvictByLastAccessedTime;


import java.util.Collection;
import java.util.Comparator;

/**
 * A implementation of the LRU algorithm that evicts the least recently used entry from any given collection of {@link EvictByLastAccessedTime}
 */
class LeastRecentUsedStrategy implements EvictionStrategy<EvictByLastAccessedTime> {

    /**
     * Execute the MRU algorithm on a collection of evictable objects.
     * @param evictables a collection of evictable objects
     */
    @Override
    public void performEviction(Collection<EvictByLastAccessedTime> evictables) {
        evictables.stream()
                  .min(Comparator.comparingLong(EvictByLastAccessedTime::getLastAccessed))
                  .ifPresent(EvictByLastAccessedTime::evict);
    }
}
