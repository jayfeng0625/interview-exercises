package com.interview.set.associative.cache.impl;

import com.interview.set.associative.cache.impl.evictable.EvictByLastAccessedTime;
import com.interview.set.associative.cache.impl.evictable.EvictByUsageFrequency;

import java.util.Objects;

/**
 * Representation of an entry to hold cached data.
 */
class Block implements EvictByLastAccessedTime, EvictByUsageFrequency {

   private Object key;
   private Object value;
   private volatile long lastCreated;
   private volatile long lastAccessed;
   private volatile int hitCount;

   /**
    * Construct an empty block
    */
   Block() {
      this(null, null);
   }

   /**
    * Construct a block that hold a key-value pair
    * @param key the key associated with the value
    * @param value the value to be stored in the entry
    */
   Block(Object key, Object value) {
      this.key = key;
      this.value = value;
   }

   synchronized Object getKey() {
      return key;
   }

   synchronized void setKey(Object key) {
      this.key = key;
   }

   synchronized Object getValue() {
      return value;
   }

   synchronized void setValue(Object value) {
      this.value = value;
   }

   synchronized long getLastCreated() {
      return lastCreated;
   }

   synchronized void setLastCreated(long lastCreated) {
      this.lastCreated = lastCreated;
   }

   @Override
   public synchronized long getLastAccessed() {
      return lastAccessed;
   }

   synchronized void setLastAccessed(long lastAccessed) {
      this.lastAccessed = lastAccessed;
   }

   @Override
   public synchronized int getUsageCount() {
      return hitCount;
   }

   synchronized void increaseHitCount() {
      hitCount++;
   }

   synchronized boolean isEmpty() {
      return key == null && value == null;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Block block = (Block) o;
      return Objects.equals(key, block.key) &&
              Objects.equals(value, block.value) &&
              Objects.equals(lastAccessed, block.lastAccessed) &&
              Objects.equals(lastCreated, block.lastCreated);
   }

   @Override
   public int hashCode() {
      return Objects.hash(key, value, lastAccessed, lastCreated);
   }

   /**
    * Empties the key and value in this block
    */
   @Override
   public void evict() {
      this.key = null;
      this.value = null;
   }

}
