package com.interview.set.associative.cache.base;

import java.util.Collection;

/**
 * Interface for eviction strategy, can be used to evict item that are {@link Evictable}
 * @param <E> types of {@link Evictable}
 */
public interface EvictionStrategy<E extends Evictable> {

   /**
    * Execute the algorithm that performs the eviction on any given type of collection storing evictable objects.
    * Note that the collection being accepted here is expected to be immutable, any modification on the collection
    * itself is likely to trigger an {@link UnsupportedOperationException}
    */
   void performEviction(Collection<E> evictables);
}
