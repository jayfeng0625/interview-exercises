package com.interview.set.associative.cache.base;

/**
 * Interface for object that can be evicted
 */
public interface Evictable {

   /**
    * specify the behaviour when this object is being evicted.
    */
   void evict();
}
