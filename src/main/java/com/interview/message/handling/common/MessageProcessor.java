package com.interview.message.handling.common;

import java.util.List;

public interface MessageProcessor<T> {

    /**
     * Process a list of messages and return them in new format.
     *
     * @param rawMessages
     *            the raw messages
     * @return the list of processed messages.
     */
    List<T> process(List<T> rawMessages);

}
