package com.interview.message.handling.common;

public interface CommonProperties {
    String FIELD_SEP = ", ";
    String SECTION_SEP = " : ";
}
