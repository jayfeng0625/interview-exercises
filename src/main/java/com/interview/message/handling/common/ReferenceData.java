package com.interview.message.handling.common;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * The Class ReferenceData provides helper methods for looking up reference data detais.
 */
public interface ReferenceData {

    /** Exchange Rate Map */
    Map<String, Double> CCY_RATE = ImmutableMap.<String, Double> builder()
            .put("USD/GBP", 0.9)
            .put("USD/EUR", 0.72)
            .put("EUR/USD", 1.388)
            .put("EUR/GBP", 1.25)
            .put("GBP/USD", 1.111)
            .put("GBP/EUR", 0.8)
            .build();

    Map<String, String> COMPANIES = ImmutableMap.<String, String> builder()
            .put("AE", "Acme Enterprises")
            .put("BS", "Big Search Company")
            .put("NILE", "Super Big Online Retailer")
            .build();

    Map<String, String> CURRENCIES = ImmutableMap.<String, String> builder()
            .put("USD", "US Dollar")
            .put("GBP", "Great British Pound")
            .put("EUR", "Euro")
            .build();

    /**
     * Gets the exchange rate when supplied with a currency pair in the form CCY/CCY
     *
     * @param currencyPair
     *            the currency pair
     * @return the exchange rate
     */
    Double getExchangeRate(final String currencyPair);
 // @formatter:off
    /**
     * Currencies data -------------------
     * USD: US Dollar
     * GBP: Great British
     * Pound EUR: Euro
     */
    // @formatter:on

    /**
     * Gets the currency code when give a currency description
     *
     * @param ccyDescription
     *            the ccy description
     * @return the currency code
     */
    String getCurrencyCode(final String ccyDescription);

    /**
     * Gets the currency description when give the ccy code.
     *
     * @param ccyCode
     *            the ccy code
     * @return the currency description
     */
    String getCurrencyDescription(final String ccyCode);

    // @formatter:off
    /**
     * <pre>
     * Company data -------------------
     * AE: Acme Enterprises
     * BS: Big Search Company
     * NILE: Super Big Online Retailer
     */
 // @formatter:on

    /**
     * Gets the company code give a company name.
     *
     * @param companyName
     *            the company name
     * @return the company code
     */
    String getCompanyCode(final String companyName);

    /**
     * Gets the company name given a company code.
     *
     * @param companyCode
     *            the company code
     * @return the company name
     */
    String getCompanyName(final String companyCode);

}
