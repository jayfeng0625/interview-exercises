package com.interview.message.handling;

import com.interview.message.handling.service.JsonToStringMessageProcessor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class MessageProcessingApplication {

    private static final List<String> RAW_MESSAGES = new ArrayList<>();
    static {
        RAW_MESSAGES.add(
                "{\"Person\":{\"firstName\":\"Donal\",\"lastName\":\"Leader\",\"dateOfBirth\":\"19900501\",\"salary\":{\"currency\":\"Euro\",\"amount\":30000.0},\"address\":{\"number\":102,\"street\":\"Champs Elysees\",\"town\":\"8eme arrondissement\",\"postCode\":\"75008\"},\"companyCode\":\"AE\"}}");
        RAW_MESSAGES.add(
                "{\"Person\":{\"firstName\":\"John\",\"lastName\":\"Smith\",\"dateOfBirth\":\"19911215\",\"salary\":{\"currency\":\"Great British Pound\",\"amount\":32000.0},\"address\":{\"number\":1,\"street\":\"Buckingham Palace\",\"town\":\"Westminster\",\"postCode\":\"SW1A 1AA\"},\"companyCode\":\"BS\"}}");
        RAW_MESSAGES.add(
                "{\"Person\":{\"firstName\":\"AB\",\"lastName\":\"Other\",\"dateOfBirth\":\"19850315\",\"salary\":{\"currency\":\"Euro\",\"amount\":32000.0},\"address\":{\"number\":88,\"street\":\"Grafton Street\",\"town\":\"\",\"postCode\":\"D02 VF65\"},\"companyCode\":\"BS\"}}");
        RAW_MESSAGES.add(
                "{\"Person\":{\"firstName\":\"Bob\",\"lastName\":\"Builder\",\"dateOfBirth\":\"19790716\",\"salary\":{\"currency\":\"US Dollar\",\"amount\":35000.0},\"address\":{\"number\":1560,\"street\":\"Broadway\",\"town\":\"New York\",\"postCode\":\"NY 10036\"},\"companyCode\":\"AE\"}}");
        RAW_MESSAGES.add(
                "{\"Person\":{\"firstName\":\"Frank\",\"lastName\":\"DePlume\",\"dateOfBirth\":\"19950320\",\"salary\":{\"currency\":\"US Dollar\",\"amount\":38000.0},\"address\":{\"number\":317,\"street\":\"S Broadway\",\"town\":\"Los Angeles\",\"postCode\":\"CA 90013\"},\"companyCode\":\"NILE\"}}");
    }

    public static void main(final String[] args) {
        SpringApplication.run(MessageProcessingApplication.class, args);
    }

    @Bean
    public CommandLineRunner run(JsonToStringMessageProcessor messageProcessor) {
        return args -> {
            final List<String> processedMessages = messageProcessor.process(RAW_MESSAGES);

            // print out in descending order of salary converted to GBP, using the given exchange rate
            System.out.println("\n------------------------------------------------------------------------OUTPUI STARTS------------------------------------------------------------------------");
            for (final String message : processedMessages) {
                System.out.println(message);
            }
            System.out.println("-------------------------------------------------------------------------OUTPUI ENDS-------------------------------------------------------------------------\n");
        };
    }
}