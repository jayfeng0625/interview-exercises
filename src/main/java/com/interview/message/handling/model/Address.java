package com.interview.message.handling.model;

import lombok.Value;

@Value
public class Address {
    int number;
    String street;
    String town;
    String postCode;

    public String getLineOne() {
        return number + " " + street;
    }
}
