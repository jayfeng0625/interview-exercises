package com.interview.message.handling.model.message;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import static com.interview.message.handling.common.CommonProperties.FIELD_SEP;
import static com.interview.message.handling.common.CommonProperties.SECTION_SEP;

@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class Header {

    int messageLength;
    int messageNumber;
    int passwordLength;
    String encryptedPassword;

    @Override
    public String toString() {
        return  "1=IOI" + FIELD_SEP +
                "2=Algomi" + FIELD_SEP +
                "3=" + messageLength + FIELD_SEP +
                "4=" + messageNumber + FIELD_SEP +
                "5=" + passwordLength + FIELD_SEP +
                "6=" + encryptedPassword + SECTION_SEP;
    }
}
