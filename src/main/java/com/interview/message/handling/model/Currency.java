package com.interview.message.handling.model;

import lombok.Value;

@Value
public class Currency {
    String code;
    String name;
}
