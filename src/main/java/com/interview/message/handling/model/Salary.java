package com.interview.message.handling.model;

import lombok.Value;

@Value
public class Salary {
    String currency;
    double amount;
}
