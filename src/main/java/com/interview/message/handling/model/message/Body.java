package com.interview.message.handling.model.message;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import static com.interview.message.handling.common.CommonProperties.FIELD_SEP;
import static com.interview.message.handling.common.CommonProperties.SECTION_SEP;

@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class Body {
    String name;
    int age;
    String companyName;
    String addressLine1;
    String addressLine2;
    String postCode;
    double salary;
    String ccy;

    @Override
    public String toString() {
        return  name + FIELD_SEP +
                age + FIELD_SEP +
                companyName + FIELD_SEP +
                addressLine1 + FIELD_SEP +
                addressLine2 + FIELD_SEP +
                postCode + FIELD_SEP +
                salary + FIELD_SEP +
                ccy + SECTION_SEP;
    }
}
