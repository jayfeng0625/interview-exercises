package com.interview.message.handling.model;

import lombok.Value;

@Value
public class Company {
    String code;
    String name;
}
