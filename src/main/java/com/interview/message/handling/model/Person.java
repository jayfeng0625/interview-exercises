package com.interview.message.handling.model;

import lombok.Value;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

@Value
public class Person {
    String firstName;
    String lastName;
    String dateOfBirth;
    Salary salary;
    Address address;
    String companyCode;

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public int calculateAge() {
        LocalDate dob = LocalDate.parse(dateOfBirth, DateTimeFormatter.ofPattern("yyyyMMdd"));
        return Period.between(dob, LocalDate.now()).getYears();
    }
}
