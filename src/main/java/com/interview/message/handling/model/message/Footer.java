package com.interview.message.handling.model.message;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class Footer {
    int bodyHashcode;
    int bodyLength;

    @Override
    public String toString() {
        return "100=" + checkSum();
    }

    private int checkSum() {
        return (bodyHashcode + bodyLength * 31) % 1048;
    }
}
