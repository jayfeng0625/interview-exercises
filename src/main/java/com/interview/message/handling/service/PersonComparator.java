package com.interview.message.handling.service;

import com.interview.message.handling.common.ReferenceData;
import com.interview.message.handling.model.Person;
import com.interview.message.handling.model.Salary;
import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
public class PersonComparator implements Comparator<Person> {

    private static final String REFERENCE_CURRENCY = "GBP";
    private final ReferenceData referenceData;

    public PersonComparator(ReferenceData referenceData) {
        this.referenceData = referenceData;
    }

    @Override
    public int compare(Person a, Person b) {
        Salary aSalary = convertToReferenceCurrency(a.getSalary(), REFERENCE_CURRENCY);
        Salary bSalary = convertToReferenceCurrency(b.getSalary(), REFERENCE_CURRENCY);
        return Double.compare(aSalary.getAmount(), bSalary.getAmount());
    }

    private Salary convertToReferenceCurrency(Salary salary, String referenceCurrency) {
        String currentCurrencyCode = referenceData.getCurrencyCode(salary.getCurrency());
        if (currentCurrencyCode.equals(referenceCurrency)) {
            return new Salary(referenceData.getCurrencyDescription(referenceCurrency), salary.getAmount());
        }
        String currencyPair = currentCurrencyCode + "/" + referenceCurrency;
        return new Salary(
                referenceData.getCurrencyDescription(referenceCurrency),
                salary.getAmount() / referenceData.getExchangeRate(currencyPair));
    }
}
