package com.interview.message.handling.service;

import com.interview.message.handling.common.ReferenceData;
import com.interview.message.handling.model.Person;
import com.interview.message.handling.model.message.Body;
import com.interview.message.handling.model.message.Footer;
import com.interview.message.handling.model.message.Header;
import com.interview.message.handling.util.Cipher;
import org.springframework.stereotype.Component;

@Component
public class MessageEnricher {

    private static final JacksonJSON JSON_DESERIALIZER = JacksonJSON.INSTANCE;

    private final ReferenceData referenceData;
    private static final String PASSWORD = "Password";
    private static final int K = 3;

    public MessageEnricher(ReferenceData referenceData) {
        this.referenceData = referenceData;
    }

    public String getEnrichedMessage(Person person, int messageNumber) {
        Header header = buildHeader(person, messageNumber);
        Body body = buildBody(person);
        Footer footer = buildFooter(body);

        return String.valueOf(header) + body + footer;
    }

    private Header buildHeader(Person person, int messageNumber) {
        String encryptedPassword = Cipher.caesarCipher(PASSWORD, K);
        return Header.builder()
                .messageLength(JSON_DESERIALIZER.toJsonString(person).getBytes().length)
                .messageNumber(messageNumber)
                .encryptedPassword(encryptedPassword)
                .passwordLength(encryptedPassword.length())
                .build();
    }

    private Footer buildFooter(Body body) {
        return Footer.builder()
                .bodyHashcode(body.hashCode())
                .bodyLength(JSON_DESERIALIZER.toJsonString(body).getBytes().length)
                .build();
    }

    private Body buildBody(Person person) {
        return Body.builder()
                .name(person.getFullName())
                .age(person.calculateAge())
                .companyName(referenceData.getCompanyName(person.getCompanyCode()))
                .addressLine1(person.getAddress().getLineOne())
                .addressLine2(person.getAddress().getTown())
                .postCode(person.getAddress().getPostCode())
                .salary(person.getSalary().getAmount())
                .ccy(referenceData.getCurrencyCode(person.getSalary().getCurrency()))
                .build();
    }
}
