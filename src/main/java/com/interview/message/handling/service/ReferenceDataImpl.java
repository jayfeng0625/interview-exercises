package com.interview.message.handling.service;

import com.interview.message.handling.common.ReferenceData;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ReferenceDataImpl implements ReferenceData {

    /**
     * Gets the exchange rate when supplied with a currency pair in the form CCY/CCY
     *
     * @param currencyPair the currency pair
     * @return the exchange rate
     */
    @Override
    public Double getExchangeRate(String currencyPair) {
        return CCY_RATE.get(currencyPair);
    }

    /**
     * Gets the currency code when give a currency description
     *
     * @param ccyDescription the ccy description
     * @return the currency code
     */
    @Override
    public String getCurrencyCode(String ccyDescription) {
        return CURRENCIES.entrySet().stream()
                .filter(entry -> entry.getValue().equals(ccyDescription))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(ccyDescription + " is not presented in map"));
    }

    /**
     * Gets the currency description when give the ccy code.
     *
     * @param ccyCode the ccy code
     * @return the currency description
     */
    @Override
    public String getCurrencyDescription(String ccyCode) {
        String result = CURRENCIES.get(ccyCode);
        if (result == null) {
            throw new IllegalArgumentException(ccyCode + " is not presented in map");
        }
        return result;
    }

    /**
     * Gets the company code give a company name.
     *
     * @param companyName the company name
     * @return the company code
     */
    @Override
    public String getCompanyCode(String companyName) {
        return COMPANIES.entrySet().stream()
                .filter(entry -> entry.getValue().equals(companyName))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(companyName + " is not presented in map"));
    }

    /**
     * Gets the company name given a company code.
     *
     * @param companyCode the company code
     * @return the company name
     */
    @Override
    public String getCompanyName(String companyCode) {
        String result = COMPANIES.get(companyCode);
        if (result == null) {
            throw new IllegalArgumentException(companyCode + " is not presented in map");
        }
        return result;
    }
}
