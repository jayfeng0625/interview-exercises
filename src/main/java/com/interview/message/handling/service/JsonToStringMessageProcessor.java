package com.interview.message.handling.service;

import com.interview.message.handling.common.MessageProcessor;
import com.interview.message.handling.model.Person;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
public final class JsonToStringMessageProcessor implements MessageProcessor<String> {

    private static final JacksonJSON JSON_DESERIALIZER = JacksonJSON.INSTANCE;

    private final MessageEnricher messageEnricher;
    private final PersonComparator personComparator;
    private final AtomicInteger counter = new AtomicInteger(0);

    /**
     * Instantiates a new json to string message processor.
     * @param messageEnricher the instance of MessageEnricher
     */
    public JsonToStringMessageProcessor(final MessageEnricher messageEnricher,
                                        final PersonComparator personComparator) {
        this.messageEnricher = messageEnricher;
        this.personComparator = personComparator;
    }

    /**
     * Process a list of messages and return them in new format.
     *
     * @param rawMessages
     *            the raw messages
     * @return the list of processed messages.
     */
    @Override
    public List<String> process(final List<String> rawMessages) {
        counter.set(0);
        return rawMessages.stream()
                .map(rawMessage -> JSON_DESERIALIZER.fromJsonString(rawMessage, Person.class))
                .sorted(personComparator.reversed())
                .map(person -> messageEnricher.getEnrichedMessage(person, counter.incrementAndGet()))
                .collect(Collectors.toList());
    }

}
