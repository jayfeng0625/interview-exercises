package com.interview.message.handling.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;

/**
 * The Enum JacksonJSON.
 */
public enum JacksonJSON {

    /** The instance. */
    INSTANCE;

    private ObjectMapper mapper;
    private ObjectWriter writer;

    /**
     * Instantiates a new jackson json Converter.
     */
    JacksonJSON() {
        this.mapper = new ObjectMapper();
        this.writer = this.mapper.writerWithDefaultPrettyPrinter();
        this.mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        this.mapper.disable(SerializationFeature.WRITE_NULL_MAP_VALUES);
        this.mapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
        this.mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
    }

    public final ObjectMapper mapper() {
        return this.mapper;
    }

    /**
     * To json string.
     *
     * @param object
     *            the object
     * @return the string
     */
    public final String toJsonString(final Object object) {
        String objAsSting = null;
        try {
            objAsSting = this.writer.withView(this.getClass())
                    // .withDefaultPrettyPrinter()
                    .with(SerializationFeature.WRAP_ROOT_VALUE)
                    .writeValueAsString(object);

        } catch (final JsonProcessingException jpe) {
            System.out.printf("Error serializing entity object to Json");
            jpe.printStackTrace();
        }
        return objAsSting;
    }

    /**
     * From json string.
     *
     * @param <T>
     *            the generic type generated object
     * @param jsonString
     *            the json string
     * @param clazz
     *            the json string object
     * @return the object
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public final <T> T fromJsonString(final String jsonString, final Class<T> clazz) {
        T bean = null;
        try {
            bean = this.mapper.readValue(jsonString, clazz);
        } catch (final IOException up) {
            System.out.println("Error reading value from jsonString");
            up.printStackTrace();
        }
        return bean;
    }
}
