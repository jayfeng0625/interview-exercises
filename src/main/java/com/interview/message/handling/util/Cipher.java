package com.interview.message.handling.util;

public class Cipher {

    public static String caesarCipher(String original, int shift) {
        StringBuilder builder = new StringBuilder();
        original.chars()
                .filter(Character::isAlphabetic)
                .forEach(value -> shiftCharacterAndAppend(shift, value, builder));

        return builder.toString();
    }

    private static void shiftCharacterAndAppend(int shift, int value, StringBuilder builder) {
        char a = Character.isUpperCase(value) ? 'A' : 'a';
        builder.append(Character.toChars((value - a + shift) % 26 + a));
    }
}
