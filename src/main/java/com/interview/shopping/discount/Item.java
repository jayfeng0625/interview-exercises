package com.interview.shopping.discount;

public class Item {
    public enum ItemType {
        Food, Clothing, DVD
    }

    private final String name;
    private final ItemType type;
    private final float price;

    public Item(String name, ItemType type, float price) {
        this.name = name;
        this.type = type;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public ItemType getType() {
        return type;
    }

    public float getPrice() {
        return price;
    }
}
