package com.interview.shopping.discount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Basket {

    private List<Item> items = new ArrayList<>();

    public Basket(Item... items) {
        Collections.addAll(this.items, items);
    }

    public List<Item> getItems() {
        return items;
    }
}
