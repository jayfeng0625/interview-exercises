package com.interview.shopping.discount;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SmartPricer implements Pricer {

    private static final float TAX_RATE = 0.2f;

    @Override
    public float price(Basket basket) {
        float sum = 0f;

        List<Item> items = basket.getItems();
        List<Item> twoForOneItems = new ArrayList<>();

        for (Item item : items) {
            switch (item.getType()) {
                case Clothing:
                    sum += item.getPrice() + item.getPrice() * TAX_RATE;
                    break;
                case DVD:
                    twoForOneItems.add(item);
                    // falls through as we still wants to add up the price
                default:
                    sum += item.getPrice();
                    break;
            }
        }

        // sort the price as we only want to deduct the cheapest
        twoForOneItems.sort(Comparator.comparing(Item::getPrice));
        for (int i = 0, len = twoForOneItems.size() / 2; i < len; i++) {
            sum -= twoForOneItems.get(i).getPrice();
        }

        return new BigDecimal(sum).setScale(2, RoundingMode.HALF_UP).floatValue();
    }
}
